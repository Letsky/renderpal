# ******************************************************************************
# RpQuery.py - RenderPal V2 Query script for Python
# Copyright (c) Shoran Software
# ******************************************************************************

# The module 'RpQuery' is the only one you need to import
from RpQuery import *
import os

PROJECT_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
rcPath = os.path.join(PROJECT_ROOT,'rprccmd')
RENDER_PAL_SERVER_ADDRESS = '192.168.1.6'
RENDER_PAL_SERVER_PORT = '7506'
RENDER_PAL_USERNAME = 'abomariam'
RENDER_PAL_PASSWORD = 'mahmoud'


def example1():
	# This example will simply query all net jobs and print their attributes.
	# These attributes represent the various values fetched from the query;
	# this is a good way to find out what each object has to offer.

	query	= RpQuery(
        rcPath=rcPath,
        argServer=RENDER_PAL_SERVER_ADDRESS,
        argUserName=RENDER_PAL_USERNAME,
        argPassword=RENDER_PAL_PASSWORD,
        argServerPort=RENDER_PAL_SERVER_PORT
    ) # You may also specify an optional path to the Remote Controller
	netJobs	= query.getAllNetJobs() # You may also pass an optional boolean, which tells the function whether you want to fetch all chunks as well

	if netJobs != None: # If a getXXX function returns None, the query failed
		for netJob in netJobs: # netJobs contains an array of RpNetJob objects
			print netJob.Name + ": " + str(dir(netJob)) # RpNetJob.Name is one of the attributes taken from the query

def example2():
	# This example will fetch a specific pool (we use the 'Default pool' here; if
	# you no longer have this pool, change the name accordingly), including its
	# assigned clients.

	query	= RpQuery(rcPath=rcPath) # You may also specify an optional path to the Remote Controller
	pool	= query.getPool("Default pool", True) # First arg=Pool name, Second arg=Get clients

	if pool != None: # Always perform this check
		print pool.Name + ":"
		# Now enumerate all clients
		for client in pool.Clients:
			print "\t" + client.Alias

def example3():
	# This example will fetch all net jobs, including their chunks, and
	# print several details about each chunk. It basically is a more complex
	# version of the first example.

	query	= RpQuery(rcPath=rcPath)
	netJobs	= query.getAllNetJobs(True) # Include chunks as well

	if netJobs != None:
		for netJob in netJobs:
			print netJob.Name + " (" + str(netJob.NetJobID) + "), " + str(len(netJob.Chunks)) + " chunks:"
			# Iterate over its chunks
			for chunk in netJob.Chunks:
				print "-- Chunk " + str(chunk.ChunkID) + ", " + chunk.Frames + ": " + chunk.StatusText

def main():
	# print "--- Running example 1 - Fetching all net jobs ---\n"
	example1()
	# print "\n--- Running example 2 - Fetching a pool and its clients ---\n"
	example2()
	# print "\n--- Running example 3 - Listing net job chunk details ---\n"
	example3()

if __name__ == '__main__':
	main()
