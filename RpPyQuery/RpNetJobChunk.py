# ******************************************************************************
# RpNetJobChunk.py - RenderPal V2 Net Job Chunk wrapper
# Copyright (c) Shoran Software
# ******************************************************************************

from RpXmlObj import *

class RpNetJobChunk(RpXmlObject):
	"""Class to wrap a RenderPal V2 Net Job Chunk"""
	
#-- ctor
	def __init__(self, domNode=None):
		RpXmlObject.__init__(self, domNode)
	