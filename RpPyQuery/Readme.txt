**********************************************
* RenderPal V2 Python Query 0.1 - Readme.txt *
* Copyright (c) Shoran Software              *
**********************************************

-- Part A: General information
------------------------------

A1. SYNOPSIS
============

The RenderPal V2 Python Query library is a wrapper for the RenderPal V2 Remote Controller. It's
main purpose is to ase the integration of the Remote Controller into Python-based rendering
pipelines. This readme will explain the API of the PyQuery library; a decent knowledge of the
Python scripting language is required.

The entire library can also be seen as a good example of how the Remote Controller can be used,
how to work with its query feature and so on.

A2. VERSION HISTORY
===================

0.1 - First release with basic functionality

A3. PREREQUISITES
=================

The PyQuery library is written for Python 2.5 and newer; you will need either a separate Python
installation, or a host application that has Python integrated. This also means that you can
theoretically use the PyQuery library from within RenderPal V2 itself. All module files belonging
to PyQuery (except Example.py) are required and should be extracted to a location where Python
will be able to find them.

The PyQuery library uses the console-version of the RenderPal V2 Remote Controller; an
installation of the console RC is thus required (you will need at least version 2.5.0).

-- Part B: API Organization
---------------------------

B1. MODULES
===========

The PyQuery library consists of several modules; most of these are used internally only, but
there are a few you might want to customize and extend to your own needs (which is highly
encouraged). Here is a brief overview of the included modules:

- RpQuery.py:
	This is the main library module and the one you will use most of the time. The class
	"RpQuery" is the interface you will use to query data from the server. Full explanation of its
	API follows below.
	
- Example.py:
	This module contains a few simple examples to get you started.
	
- RpRemCtrl.py:
	This is a direct wrapper for the console RC executable. The "RpRemoteController" is usually
	used as a base class for the various parts of the API, like the RpQuery class. The class only
	offers few methods, which will also be described below.
	
- RpXmlObj.py:
	This module contains the base class for all "query objects"; its main task is to create
	instance attributes from the XML data returned by the Remote Controller.
	
- RpNetJob.py, RpNetJobChunk.py, RpPool.py, RpClient.py:
	These modules define the various "query objects". If you take a look at them, you will
	notice that most do not really offer any own functionality (they all inherit from RpXmlObject).
	However, these classes can be easily customized to your own needs.
	
- RpUtils.py:
	This module only contains a few helper functions you will usually not need yourself.
	
Customizations should only be made in the various query object modules; if you want to do more
advanced things, you should write your own class that derives from RpRemoteController - just
take a look how the RpQuery class is built.

B2. GENERAL USAGE
=================

Using the PyQuery API has been made as simple and as straightforward as possible. Depending on
which feature you want to use (currently, the PyQuery API only supports query functionality),
you will create an instance of the corresponding class and call its various methods:

- RpQuery: Querying the server:
	The RpQuery class can be used to send queries to the server; the server will then send the
	requested objects (as XML data). You will not need to work with the returned XML data directly -
	this is all done under the hood for you: The RpQuery class will return objects that will contain
	various attributes representing the returned data.
	
	The general usage of this class is:
		query = RpQuery([optional path to the console RC])
		object/objectList = query.getXXX(...)
		do something with the objects returned
		
	Details about the RpQuery API will follow.

-- Part C: API Reference
------------------------

C1. THE RPREMOTECONTROLLER CLASS
================================

The RpRemoteController class is the base class for all higher features of the PyQuery library;
all classes you will use to operate the RC will inherit from RpRemoteController, and thus its
API should be handled first.

def __init__(rcPath=""[String]):
	The constructor takes an optional path to the console Remote Controller (all descendants of
	this class have the same constructor). If no path is specified, the constructor will use the
	RP_CMDRC_DIR environment variable (which is set by the console RC automatically).
	
def setServer(serverAddress[String]):
	This method can be used to specify a server address; if you do not use this, the address
	specified in the console RC configuration file will be used. The serverAddress should be a
	string in the form of "Address[:Port]".
	
def setLogin(serverLogin[String]):
	This method can be used to specify a server login; if you do not use this, the login
	specified in the console RC configuration file will be used. The serverLogin should be a string
	in the form of "User[:Password]".
	
def[Tuple:(Boolean,Array:String)] execute(args[Dictionary], compact=True[Boolean]):
	Launches the remote controller with the specified arguments "args" (args is a dictionary
	where the key is the switch and the value its parameter). If "compact" is set to True, the RC
	compact mode will be enabled. This function returns a tuple of two values: The first one is a
	boolean value indicating a success (True) or a failure (False); the second one is an array of
	strings - the textual output of the RC.
	
C2. THE RPQUERY CLASS
=====================

The RpQuery class is used to send queries to the server. This class offers a bunch of getXXX
methods that will return one or several objects (in an array). All these objects inherit from
the RpXmlObject class; every object contains several attributes that hold the data retrieved
from the server (some objects also have a list of child objects: the RpNetJob class holds a list
of its Chunks, the RpPool class holds a list of its Clients). We cannot list all attributes here
(they are also subject to frequent changes); to get a list of what an object has to offer, use
the "dir" command: print dir(obj)

Note: All getXXX methods will return None if they fail (usually since either the requested
object doesn't exist or the Remote Controller couldn't be launched).

-- Net job methods:

def[Array:RpNetJob] getNetJobs(idList [Tuple/Array:Integers], includeChunks=False [Boolean]):
	This method returns a list of RpNetJob objects. The parameter "idList" should be a tuple or
	array containing one or more net job IDs that you want to retrieve; passing None will return all
	net jobs. If "includeChunks" is set to True, the chunks of the net jobs will also be retrieved
	and assigned to the corresponding net job object (retrievable through the RpNetJob.Chunks array
	attribute).
	
def[RpNetJob] getNetJob(id[Integer], includeChunks=False[Boolean]):
	This method will return the specified net job (if it exists); see above for details about
	the "includeChunks" parameter.
	
def[Array:RpNetJob] getAllNetJobs(includeChunks=False[Boolean]):
	This method will simply retrieve all net jobs from the server.
	
-- Net job chunk methods:
	
def[Array:RpNetJobChunk] getNetJobChunks(idList[Tuple/Array:Integers]):
	This method will return a list of all chunks for the specified net jobs. The parameter
	"idList" should be a tuple or array containing one or more net job IDs that you want to retrieve
	the chunks of; passing None will return all net jobs chunks. 
	
def[Array:RpNetJobChunk] getNetJobChunksForJob(id[Integer]):
	This method will return all chunks of the specified net job.
	
def[Array:RpNetJobChunk] getAllNetJobChunks():
	This method will simply retrieve all net job chunks from the server.
	
-- Pool methods:

def[Array:RpPool] getPools(idList[Array/Tuple:Strings], includeClients=False[Boolean]):
	This method returns a list of RpPool objects. The parameter "idList" should be a tuple or
	array containing one or more pool names that you want to retrieve; passing None will return all
	pools. If "includeClients" is set to True, the clients assigned to the pool will also be
	retrieved and assigned to the corresponding pool object (retrievable through the RpPool.Clients
	array attribute).
	
def[RpPool] getPool(id[String], includeClients=False[Boolean]):
	This method will return the specified pool (if it exists); see above for details about the
	"includeClients" parameter.
	
def[Array:RpPool] getAllPools(includeClients=False[Boolean]):
	This method will simply retrieve all pools from the server.
	
def[Array:RpClient] getPoolClients(id[String]):
	This method will return a list of all clients assigned to the specified pool. Same as
	"getClientsOfPool".
	
-- Client methods:

def[Array:RpClient] getClients(idList[Array/Tuple:String]):
	This method will return a list of clients. The parameter "idList" should be a tuple or array
	containing one or more client names that you want to retrieve; passing None will return all
	clients.
	
def[RpClient] getClient(id[String]):
	This method will return the specified client (if it exists).
	
def[Array:RpClient] getClientsOfPool(pool[String/RpPool]):
	This method will return a list of all clients assigned to the specified pool. Same as
	"getPoolClients".
	
def[Array:RpClient] getAllClients():
	This method will simply return all clients.
	
-- Part D: Getting started
--------------------------

First of all, take a look at Example.py - this module contains a few examples that show you how
to use the PyQuery library. If you want to dig a bit deeper, feel free to take a look at the
other modules. Everything is commented thoroughly, so you should quickly get into the inner
workings of PyQuerry. Our code should be a good starting point if you intend to extend our
library or write your own.
