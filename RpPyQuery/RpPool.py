# ******************************************************************************
# RpPool.py - RenderPal V2 Client Pool wrapper
# Copyright (c) Shoran Software
# ******************************************************************************

from RpXmlObj import *

class RpPool(RpXmlObject):
	"""Class to wrap a RenderPal V2 Client Pool"""

#-- ctor
	def __init__(self, domNode=None):
		RpXmlObject.__init__(self, domNode)

		# Will contain the various clients assigned to this pool (if they're fetched)
		# Ensures that this attribute is always available
		self.Clients = []

#-- Client methods
	def getClients(self): return self.Clients
	def getClient(self, index): return self.Clients[index]
	def getClientCount(self): return len(self.Clients)
