# ******************************************************************************
# RpNetJob.py - RenderPal V2 Net Job wrapper
# Copyright (c) Shoran Software
# ******************************************************************************

from RpXmlObj import *

class RpNetJob(RpXmlObject):
	"""Class to wrap a RenderPal V2 Net Job"""
	
#-- ctor
	def __init__(self, domNode=None):
		RpXmlObject.__init__(self, domNode)
		
		# Will contain the various chunks belonging to this net job (if they're fetched)
		# Ensures that this attribute is always available
		self.Chunks = []
	
#-- Chunk methods
	def getChunks(self): return self.Chunks
	def getChunk(self, index): return self.Chunks[index]
	def getChunkCount(self): return len(self.Chunks)
