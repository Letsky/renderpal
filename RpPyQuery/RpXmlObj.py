# ******************************************************************************
# RpXmlObj.py - RenderPal V2 XML-based objects
# Copyright (c) Shoran Software
# ******************************************************************************

import xml.dom.minidom

class RpXmlObject(object):
	"""This class parses an XML object and creates several class attributes to hold the various values found in the XML portion"""
	
#-- ctor	
	def __init__(self, domNode=None):
		if domNode != None:
			self.assignNode(domNode)
			
#-- Node assignment
	def assignNode(self, domNode):
		"""Assigns the document node, reading its various values"""
		# XML node has to be valid
		assert domNode != None
		
		# Get the ID attribute
		if domNode.hasAttribute("id"):
			self.ObjectID = domNode.getAttribute("id")
			
		# Now walk the various children of the XML node
		for valNode in domNode.childNodes:
			if valNode.nodeType == valNode.ELEMENT_NODE:
				valList	= valNode.getElementsByTagName("Value")
				value	= ""
								
				if (valList != None) and (len(valList) > 0): # Multiple values in this node
					value = [self.__getNodeValue(valEntry) for valEntry in valList]				
				else: # Single value
					value = self.__getNodeValue(valNode)

				# Now set the attribute; the node name will be the attribute name
				setattr(self, valNode.nodeName, value)
				
#-- Private methods
	def __getNodeValue(self, node):
		"""Helper function to get the (textual) value of a node"""
		firstChild = node.firstChild
											
		if (firstChild != None) and (firstChild.nodeType == firstChild.TEXT_NODE):
			return firstChild.nodeValue
		
		return ""
