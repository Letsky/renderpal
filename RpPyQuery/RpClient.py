# ******************************************************************************
# RpClient.py - RenderPal V2 Render Client wrapper
# Copyright (c) Shoran Software
# ******************************************************************************

from RpXmlObj import *

class RpClient(RpXmlObject):
	"""Class to wrap a RenderPal V2 Render Client"""

#-- ctor
	def __init__(self, domNode=None):
		RpXmlObject.__init__(self, domNode)
