# ******************************************************************************
# RpRemCtrl.py - RenderPal V2 Remote Controller wrapper for Python
# Copyright (c) Shoran Software
# ******************************************************************************

import os
import subprocess

from RpUtils import *

class RpRemoteController(object):
	"""Class to wrap the RenderPal V2 Remote Controller"""

#-- ctor
	def __init__(self, rcPath="",argServer='',argUserName='',argPassword='',argServerPort=''):
		self.rcCommand	= ""

		self.argServer	= ':'.join([argServer,argServerPort])
		self.argLogin	= ':'.join([argUserName,argPassword])

		# Build the full path to the RC executable
		rcExecutable = ("RpRcCmd.exe" if rp_isWindows() else "rprccmd")

		if (rcPath == "") and ("RP_CMDRC_DIR" in os.environ):
			rcPath = os.environ["RP_CMDRC_DIR"]

		self.rcCommand = os.path.join(rcPath, rcExecutable)

		# Ensure that the RC executable exists
		if not os.path.exists(self.rcCommand):
			raise StandardError("The Remote Controller executable doesn't exist")

#-- Optional overrides
	def setServer(self, serverAddress):
		"""Set an optional server address ('IP/NAME[:Port]'). If not set, the one specified in the RC configuration file will be used."""
		self.argServer = serverAddress

	def setLogin(self, serverLogin):
		"""Set an optional user login ('Login[:Password]'). If not set, the one specified in the RC configuration file will be used."""
		self.argLogin = serverLogin

#-- Main methods
	def execute(self, args, compact=True):
		"""Launches the RC with the specified arguments (as a dictionary)"""
		success		= False
		procOutput	= []

		# Add internal settings as arguments to the command-line
		if self.argServer != "": args["-server"] = self.argServer
		if self.argLogin != "": args["-login"] = self.argLogin

		# Enable the compact mode
		if compact: args["-compact"] = ""

		# Assemble the various arguments
		cmdLine = [self.rcCommand]

		for argName in args.keys():
			cmdLine.append(argName)
			if args[argName] != "": cmdLine.append(args[argName])

		# Run the process
		try:
			process		= subprocess.Popen(cmdLine, stdout=subprocess.PIPE, universal_newlines=True)
			procOutput	= process.communicate()[0].split("\n")
			success		= (process.returncode == 0)
		except:
			pass

		return (success, procOutput)
