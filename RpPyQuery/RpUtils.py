# ******************************************************************************
# RpUtils.py - RenderPal V2 Python utility functions
# Copyright (c) Shoran Software
# ******************************************************************************

import sys

def rp_isWindows():	
    platform = sys.platform
    platform.lower()
    
    return platform.find("win") != -1
