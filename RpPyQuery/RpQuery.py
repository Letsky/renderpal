# ******************************************************************************
# RpQuery.py - RenderPal V2 Query script for Python
# Copyright (c) Shoran Software
# ******************************************************************************

import xml.dom.minidom

from RpRemCtrl import *
from RpNetJob import *
from RpNetJobChunk import *
from RpPool import *
from RpClient import *

class RpQuery(RpRemoteController):
	"""Class to fetch data from the RenderPal V2 Server"""

#-- ctor
	def __init__(self,**kwargs):
		RpRemoteController.__init__(self, **kwargs)

#-- Net Job methods
	def getNetJobs(self, idList, includeChunks=False):
		"""Fetches a list of net jobs and optionally their chunks as well"""
		return self.__getObjectsEx(RpNetJob, idList, "netjob", "NetJob", includeChunks, RpNetJobChunk, "Chunks", "chunks", "NetJobChunk")

	def getNetJob(self, id, includeChunks=False):
		"""Fetches a single net job and optionally its chunks as well"""
		netJobList = self.getNetJobs((id,), includeChunks)

		if (netJobList != None) and (len(netJobList) == 1): # We should only get one job
			return netJobList[0]

		# Failed to get the requested job
		return None

	def getAllNetJobs(self, includeChunks=False):
		"""Fetches all net jobs and optionally their chunks as well"""
		return self.getNetJobs(None, includeChunks)

#-- Net Job Chunk methods
	def getNetJobChunks(self, idList):
		"""Fetches the chunks of a list of net jobs"""
		return self.__getObjectsEx(RpNetJobChunk, idList, "chunks", "NetJobChunk")

	def getNetJobChunksForJob(self, id):
		"""Fetches the chunks of a single net job"""
		return self.getNetJobChunks((id,))

	def getAllNetJobChunks(self):
		"""Fetches all net job chunks"""
		return self.getNetJobChunks(None)

#-- Pool methods
	def getPools(self, idList, includeClients=False):
		"""Fetches a list of pools and optionally their assigned clients as well"""
		poolList = self.__getObjectsEx(RpPool, idList, "pool", "Pool")

		# Clients have to be fetched and assigned individually for each pool
		if (poolList != None) and includeClients:
			for pool in poolList:
				pool.Clients = self.getClientsOfPool(pool)

		return poolList

	def getPool(self, id, includeClients=False):
		"""Fetches a single pool and optionally its assigned clients as well"""
		poolList = self.getPools((id,), includeClients)

		if (poolList != None) and (len(poolList) == 1): # We should only get one pool
			return poolList[0]

		# Failed to get the requested pool
		return None

	def getAllPools(self, includeClients=False):
		"""Fetches all pools and optionally their assigned clients as well"""
		return self.getPools(None, includeClients)

	def getPoolClients(self, id):
		"""Same as getClientsOfPool"""
		return self.getClientsOfPool(id)

#-- Client methods
	def getClients(self, idList):
		"""Fetches a list of clients"""
		return self.__getObjectsEx(RpClient, idList, "client", "Client")

	def getClient(self, id):
		"""Fetches a single client"""
		clientList = self.getClients((id,))

		if (clientList != None) and (len(clientList) == 1): # We should only get one client
			return clientList[0]

		# Failed to get the requested client
		return None

	def getClientsOfPool(self, pool):
		"""Fetches the clients assigned to the specified pool"""
		# The function accepts a name (string) as well as an instance of RpPool
		if isinstance(pool, RpPool):
			pool = pool.Name

		# Syntax for querying clients of a specific pool
		pool = "(" + pool + ")"

		return self.__getObjects(RpClient, (pool,), "client", "Client")

	def getAllClients(self):
		"""Fetches all clients"""
		return self.getClients(None)

#-- Private methods - Object creation
	def __getObjectsEx(self, cls, idList, queryName, objectName, includeChildren=False, clsChildren=None, childAttrName=None, childQueryName=None, childObjectName=None):
		"""Fetches a list of objects and optionally their child objects as well"""
		(success, result) = self.execute({"-query": self.__getQueryString(idList, queryName, childQueryName if includeChildren else None)})

		if success: # Got a good result, now get the data
			rootNode	= self.__getQueryRootNode(result)
			objectList	= []

			# Retrieve the IDs from the returned data
			if idList == None:
				idList = self.__getQueryIdentifiers(rootNode, objectName)

			for id in idList:
				objectNode = self.__getQueryObject(rootNode, objectName, id)

				if objectNode != None:
					obj = cls(objectNode)

					if includeChildren: # Also get the children for this object
						childNodes = self.__getQueryObjects(rootNode, childObjectName, id)

						if childNodes != None:
							setattr(obj, childAttrName, [clsChildren(childNode) for childNode in childNodes])

					# Finally, add the object to our list
					objectList.append(obj)

			return objectList

		# Failed to get the requested objects
		return None

	def __getObjects(self, cls, idList, queryName, objectName):
		"""Fetches a list of objects without checking for their ID. This function simply returns all objects of the specified type included in the query."""
		(success, result) = self.execute({"-query": self.__getQueryString(idList, queryName)})

		if success: # Got a good result, now get the data
			rootNode	= self.__getQueryRootNode(result)
			childNodes	= self.__getQueryObjects(rootNode, objectName)
			objectList	= []

			for childNode in childNodes:
				obj = cls(childNode)
				objectList.append(obj)

			return objectList

		# Failed to get the requested objects
		return None

#-- Private methods - Query helpers
	def __getQueryString(self, idList, primaryName, secondaryName=None):
		"""Creates the RC query string, supporting up to two object types"""
		if idList != None:
			idListStr = ",".join(str(id) for id in idList)

			if secondaryName:
				queryString = "%s:%s;%s:%s" % (primaryName, idListStr, secondaryName, idListStr)
			else:
				queryString = "%s:%s" % (primaryName, idListStr)
		else:
			if secondaryName:
				queryString = "%s:*;%s:*" % (primaryName, secondaryName)
			else:
				queryString = "%s:*" % primaryName

		return queryString

	def __getQueryRootNode(self, queryResult):
		"""Parses the returned XML data and gets the root node (<ServerData>) from the query"""
		xmlText		= unicode("\n".join(queryResult), errors='ignore')
		xmlDoc		= xml.dom.minidom.parseString(xmlText)
		rootNodes	= xmlDoc.getElementsByTagName("ServerData")

		if (rootNodes != None) and (len(rootNodes) == 1): # There should always be only one <ServerData> node
			return rootNodes[0];

		return None # Invalid query data

	def __getQueryObjects(self, rootNode, objectType, objectID=None):
		"""Retrieves the XML nodes that correspond to the requested type; can be optionally filtered by an object ID"""
		# Valid query?
		if rootNode == None:
			return None

		objNodes = rootNode.getElementsByTagName(objectType)

		# Check if an ID is given and filter accordingly
		if objectID != None:
			objNodesFiltered = []

			for objNode in objNodes:
				if objNode.hasAttribute("id") and (objNode.getAttribute("id") == str(objectID)):
					objNodesFiltered.append(objNode)

			objNodes = objNodesFiltered

		return objNodes

	def __getQueryObject(self, rootNode, objectType, objectID=None):
		"""Returns the first XML node that corresponds to the requested type and ID"""
		objects = self.__getQueryObjects(rootNode, objectType, objectID)

		if (objects != None) and (len(objects) >= 1):
			return objects[0]

		return None;

	def __getQueryIdentifiers(self, rootNode, objectType):
		"""Assembles all object identifiers of the given type"""
		idList  = []
		objects	= self.__getQueryObjects(rootNode, objectType)

		for objNode in objects:
			if objNode.hasAttribute("id"):
				idList.append(objNode.getAttribute("id"))

		return idList
