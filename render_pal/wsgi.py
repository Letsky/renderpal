"""
WSGI config for render_pal project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""

import os
import sys

root = os.path.dirname(os.path.dirname(os.path.abspath(__file__+'/../')))

project = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

sys.path.insert(0, project)
sys.path.insert(0, os.path.join(project,'env/lib/python2.7/site-packages'))

activate_this = os.path.join(project, 'env/bin/activate_this.py')
with open(activate_this) as f:
    exec (f.read(),{'__file__':activate_this})
# execfile(activate_this, dict(__file__=activate_this))


from django.core.wsgi import get_wsgi_application

os.environ["DJANGO_SETTINGS_MODULE"] = "render_pal.settings"

application = get_wsgi_application()
