from importlib import import_module

from django.apps import AppConfig as BaseAppConfig


class AppConfig(BaseAppConfig):

    name = "render_pal"

    def ready(self):
        import_module("render_pal.receivers")
