# -*- coding: utf-8 -*-

__about__ = """
In addition to what is provided by the "zero" project, this project
provides thorough integration with django-user-accounts, adding
comprehensive account management functionality. It is a foundation
suitable for most sites that have user accounts.
"""

default_app_config = "render_pal.apps.AppConfig"


# from __future__ import absolute_import
from .celery import app as celery_app

