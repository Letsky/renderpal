from __future__ import absolute_import

from celery import shared_task
from django.core.mail import EmailMultiAlternatives
from django.template import Template, Context
from django.template.loader import get_template
import html2text

@shared_task
def send_mail(to, subject, template_name, attrs, reply_to=tuple()):
    html_text = get_template(template_name).render(attrs)
    plain_text = html2text.html2text(html_text)

    email = EmailMultiAlternatives()
    email.subject = Template(subject).render(Context(attrs))
    email.body = plain_text
    email.attach_alternative(html_text, 'text/html')
    email.to = to
    email.reply_to = reply_to
    email.send()
