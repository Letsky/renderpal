from __future__ import unicode_literals
from django.contrib.auth.models import AbstractBaseUser,BaseUserManager,PermissionsMixin
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.core.mail import send_mail
import braintree
from django.db.models.signals import post_save,post_delete

# Create your models here.
class MemberManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, name, email, password, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        if not name:
            raise ValueError('The given name must be set')
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(name=name, email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, name, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(name, email, password, **extra_fields)

    def create_superuser(self, name, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(name, email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    objects = MemberManager()

    USERNAME_FIELD = 'email'

    name = models.CharField(max_length=200)

    email = models.EmailField(unique=True)
    credits = models.IntegerField(default=0)
    braintree_customer_id = models.CharField(max_length=200,blank=True,null=True)

    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    REQUIRED_FIELDS = ['name']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_full_name(self):
        full_name = '%s' % (self.name)
        return full_name.strip()

    def get_short_name(self):
        return self.name

    def email_user(self, subject, message, from_email=None, **kwargs):
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def get_customer_id(self):
        if self.braintree_customer_id:
            return self.braintree_customer_id

        result = braintree.Customer.create({
            "email": self.email,
            "first_name":self.name
        })
        if result.is_success:
            self.braintree_customer_id = result.customer.id
            self.save()
            return self.braintree_customer_id

class Payment(models.Model):

    user = models.ForeignKey(User,null=True,blank=True,related_name='payments')

    transaction_id = models.CharField(unique=True,max_length=50)
    amount = models.DecimalField(decimal_places=2,max_digits=10,default=0.0)
    created_at = models.DateTimeField(default=timezone.now)
    status = models.CharField(max_length=50,default='')
    is_success = models.BooleanField(default=False)

    class Meta:
        ordering = ['-created_at']

    def __unicode__(self):
        return self.transaction_id


def add_credits(instance,created,**kwargs):
    if created:
        instance.user.credits += instance.amount
        instance.user.save()

post_save.connect(add_credits,Payment)

def remove_credits(instance,**kwargs):
    instance.user.credits -= instance.amount
    instance.user.save()

post_delete.connect(remove_credits,Payment)

