__author__ = 'abomariam'
from django.conf.urls import include, url
from . import views


urlpatterns = [
    url(r"^account/signup/$", views.SignupView.as_view(), name="account_signup"),
    url(r"^account/login/$", views.LoginView.as_view(), name="account_login"),
    url(r"^account/logout/$", views.LogoutView.as_view(), name="account_logout"),
    url(r"^account/confirm_email/(?P<key>\w+)/$", views.ConfirmEmailView.as_view(), name="account_confirm_email"),

    url(r"^dashboard/$", views.dashboard_home_view, name="dashboard-home"),
    url(r"^credits/$", views.credits_view, name="credits"),
    url(r"^credits/add-credits/$", views.add_credits_view, name="add-credits"),
]