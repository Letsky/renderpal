from account.hooks import AccountDefaultHookSet
from django.template.loader import render_to_string

from account.conf import settings
from main.tasks import send_mail

class AccountHookSet(AccountDefaultHookSet):
    def send_confirmation_email(self, to, ctx):
        subject = render_to_string("account/email/email_confirmation_subject.txt", ctx)
        subject = "".join(subject.splitlines())  # remove superfluous line breaks
        context = {}
        context['subject'] = subject
        context['site_name'] = ctx['current_site'].name
        context['name'] = ctx['user'].name
        context['url'] = ctx['activate_url']

        send_mail.delay(to, subject, 'email/email_confirmation_message.html', context)


    def send_password_reset_email(self, to, ctx):
        subject = render_to_string("account/email/password_reset_subject.txt", ctx)
        subject = "".join(subject.splitlines())
        context = {}
        context['subject'] = subject
        context['site_name'] = ctx['current_site'].name
        context['name'] = ctx['user'].name
        context['url'] = ctx['password_reset_url']
        send_mail(to, subject, 'email/password_reset.html', context)


    def send_password_change_email(self, to, ctx):
        subject = render_to_string("account/email/password_change_subject.txt", ctx)
        subject = "".join(subject.splitlines())
        context = {}
        context['subject'] = subject
        context['site_name'] = ctx['current_site'].name
        context['name'] = ctx['user'].name
        context['now'] = ctx['user'].account.now
        send_mail(to, subject, 'email/password_change.html', context)