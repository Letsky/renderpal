from django.shortcuts import render,redirect
from account import views as account_views
from account import forms as account_forms
from . import forms
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.contrib import auth,messages
import braintree
from django.conf import settings
from models import Payment
from decimal import Decimal
from django.core.urlresolvers import reverse

class SignupView(account_views.SignupView):
    form_class = forms.SignupForm
    identifier_field='email'

    def generate_username(self, form):
        pass

    def create_user(self, form, commit=True, model=None, **kwargs):
        User = model
        if User is None:
            User = get_user_model()
        user = User(**kwargs)

        user.name = form.cleaned_data["name"].strip()
        user.email = form.cleaned_data["email"].strip()
        password = form.cleaned_data.get("password")
        if password:
            user.set_password(password)
        else:
            user.set_unusable_password()
        if commit:
            user.save()
        return user

class LoginView(account_views.LoginView):
    form_class = account_forms.LoginEmailForm


class LogoutView(account_views.LogoutView):
    def get(self, *args, **kwargs):
        return self.post(*args,**kwargs)

@login_required
def dashboard_home_view(request):
    context = dict()
    return render(request,'dashboard/home.html',context)


class ConfirmEmailView(account_views.ConfirmEmailView):
    messages = {
        "email_confirmed": {
            "level": messages.SUCCESS,
            "text": 'Thank you for confirming you email.'
        }
    }

    def get(self, *args, **kwargs):
        return self.post(*args,**kwargs)

    def after_confirmation(self, confirmation):
        user = confirmation.email_address.user
        user.is_active = True
        user.save()
        user.backend = "account.auth_backends.EmailAuthenticationBackend"
        auth.login(self.request, user)
        confirmation.delete()



@login_required
def add_credits_view(request):
    context = {}

    context['braintree_token'] = braintree.ClientToken.generate({
        'customer_id':request.user.get_customer_id()
    })
    context['merchant_id'] = settings.BRAINTREE_MARCHANT_ID


    if request.method == 'POST':
        if not request.POST.get('amount',None):
            messages.error(request,'Please choose an amount.')
        else:

            result = braintree.Transaction.sale({
                    "amount": Decimal(request.POST.get('amount',None)),
                    'customer_id': request.user.get_customer_id(),
                    "payment_method_nonce": request.POST.get('payment_method_nonce'),
                    "options":{
                        "submit_for_settlement": True
                    }
                })
            if result.is_success:

                payment = Payment()
                payment.user = request.user
                payment.amount = result.transaction.amount
                payment.transaction_id = result.transaction.id
                payment.created_at = result.transaction.created_at
                payment.status = result.transaction.status
                payment.is_success = result.is_success
                payment.save()
                return redirect(reverse('credits'))
            else:
                messages.error(request,result.message)

    return render(request,'dashboard/add-credits.html',context)



@login_required
def credits_view(request):
    context = dict()
    context['payments'] = request.user.payments.all()
    return render(request,'dashboard/credits.html',context)

